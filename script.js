$jui = $jui || {};
$jui.juilayout = {
    onJqReady: function(data) {
        var _rszrResizing = false
        ,   _rszrOnStop = false;

        if ( !juiwindata.widgetsExists.juilayout ) {
            return;     // no layout-widgets on that page
        }

        juiwindata.oninit = true;
        delete juiwindata.waiting.juilayout;
        var layouts = $jq('.juilayout-widget', data['context']);
        layouts.each(function() {
            try {
                var lout = $jq(this);
                if (lout.data('ready') || $jui.widget.getClosestNotReady(lout)) return;

                var vars = lout.data('juivars') || {}
                ,   juiflags = lout.data('juiflags') || {}
                ,   state  = vars.state || {}
                ,   jopts  = vars.juilayout || {}
                ,   oopts  = layoutOpts(jopts.opts) || {}
                ,   fillspace = juiflags.fillspace || false
                ,   vresizer  = vars.resizer || {}
                ,   rflags    = vresizer.addFlags || {}
                ;
                if (juiflags.nostorestate) _clearState(state);
                if (rflags.nostorestate && state.resizer) delete state.resizer;
                vars.state = state;

                // *********************************************
                // create layout-object
                var instance = lout.layout(oopts);
                // *********************************************
                lout.data('self', lout);
                lout.data('instance', instance);

                var dlout   = lout.data('layout')
                ,   panes   = dlout.panes
                ,   options = dlout.options
                ,   closest = $jui.widget.getClosest(lout)
                ,   closestContent  = closest ? $jui.widget.getClosestContent(lout) : false
                ,   resizable = fillspace ? false : juiflags.resizable || false
                ,   style  = jopts.style || false
                ,   size   = $jui.resizer.getSize(state.resizer, style, fillspace)
                ,   height = style.height || false
                ,   width  = style.width  || false
                ;
                if (closest) closest = closest.data('self');
                lout.extend({panes:      panes,
                             options:    options,
                             juiwidget:  lout,
                             juistate:   state,
                             juivars:    vars,
                             juiclosest: closest,
                             juiflags:   juiflags,
                             rflags:     rflags
                });

                // add/remove class, attr, style
                // for widget, header and panels
                var head      = vars.head || {}
                ,   content   = vars.content || {}
                ,   sections  = vars.sections || {}
                ;
                $jui.widget.setAttrCss(lout, jopts);
                        //JUIFIX:: ..lout..$jui.widget.setAttrCss(headers, head);
//                        $jui.widget.setAttrCss(headers, head);
//                        $jui.widget.setAttrCss(panels, content);
//                        $jui.widget.setSectsAttrCss(headers, sections, 'head');
//                        $jui.widget.setSectsAttrCss(panels, sections, 'content');

                
                height = fillspace ? 1 : $jui.resizer.sizeExists(height) ? height : false;
                width  = fillspace ? 1 : $jui.resizer.sizeExists(width)  ? width  : false;

                var hasResizer = fillspace || resizable || height || width;
                if (hasResizer) {
                    lout.extend({hasResizer:true});
                    height = height || 1;
                    width  = width  || 1;
                    var ropts = resizable && vresizer.opts 
                                ? vresizer.opts : {disabled:true, handles:'none'}
                    ,   orszr = resizerOpts(ropts)
                    ,   funcs = resizerFuncs()
                    ,   xrszr = $jui.resizer.getExtends(size, funcs)
                    ;

                    lout.resizable(orszr).extend(xrszr).juiAddChildren();
                    lout.extend({juiresizable: lout.data('resizable')});
                    if (fillspace && closestContent) {
                        closestContent.addClass('jui-fillspace');
                    }
                    lout.juiResize();
                }

                var cc = $jui.widget.getClosestContent(lout);
                if (cc.length) cc = cc.data('self');
                $jq.each(instance.panes, function() {
                    var elem = arguments[1];
                    if ( elem ) {
                        // hooks
                        if (cc && cc.juiAjaxPreLoad) elem.juiAjaxPreLoad = cc.juiAjaxPreLoad;
                        if (cc && cc.juiAjaxPostLoad) elem.juiAjaxPostLoad = cc.juiAjaxPostLoad;
                        elem = elem.data('self', elem);
                        // ajax call if needed
                        $jui.ajax.parseContent($jui.widget.getJqElem(elem), elem);
                    }
                });

                lout.data('ready', true);
            }catch(e){ $jui.dbgAlert('juilayoutOnJqReady.each()', e.message); }
            
            function _clearState(state) {
                $jq.each('north,south,west,east'.split(','), function(i, pane) {
                    if (state[pane]) delete state[pane];
                });
            }
        });
        juiwindata.oninit = false;



        //
        // functions
        //
        function layoutOpts( opts ) {
            opts = opts || {};
            $jq.extend(true, opts, {
                onload_start: function (instance) {
                    var state = instance.container.data('juivars').state;
                    instance.loadState(state);
                },
                defaults: {
                    onshow_end: function (pane, elem, state, opts, ln) {
                        resizeChildren(elem);
                        storeState(elem, pane, 'initHidden', null);
                        $jui.ajax.parseContent($jui.widget.getJqElem(elem), elem);
                    },
                    onhide_end: function (pane, elem, state, opts, ln) {
                        resizeChildren(elem);
                        storeState(elem, pane, 'initHidden', 1);
                    },
                    onopen_end: function (pane, elem, state, opts, ln) {
                        resizeChildren(elem);
                        storeState(elem, pane, 'initClosed', null);
                        $jui.ajax.parseContent($jui.widget.getJqElem(elem), elem);
                    },
                    onclose_end: function (pane, elem, state, opts, ln) {
                        resizeChildren(elem);
                        storeState(elem, pane, 'initClosed', 1);
                    }
                },
                center: {
                    onresize_end: function (pane, elem, state, opts, ln) {
                        if (_rszrResizing) return;
                        resizeChildren(elem);
                        storeState(elem, pane, 'size', state.size);
                    }
                }

            });
            return opts;

            function resizeChildren(elem) {
                try {
                    if (juiwindata.oninit || !elem.jquery || elem.lenght==0) return;
                    var lout  = $jui.widget.getSelf($jui.widget.getJqElem(elem));
                    lout.juiResizeChildren();
                }catch(e){ $jui.dbgAlert('resizeChildren()', e.message); }
            }

            function storeState(elem, pane, key, val) {
                if (juiwindata.oninit) return;

                try {
                    var lout  = $jui.widget.getSelf($jui.widget.getJqElem(elem))
                    ,   state = lout.juistate;
                    if (state && !lout.juiflags.nostorestate) {
                        if (pane == 'center') {
                            key = 'size';
                            var panes = lout.data('instance').panes
                            ,   istat = lout.data('instance').state;
                            for (var pn in panes) {
                                if (!panes[pn]) continue;

                                val = false;
                                switch (pn) {
                                    case 'north':
                                    case 'south':
                                        val = istat[pn].outerHeight;
                                        break;
                                    case 'west':
                                    case 'east':
                                        val = istat[pn].outerWidth;
                                        break;
                                    default:
                                        continue;
                                }
                                state[pn] = state[pn] || {};
                                if (typeof val === 'number') {
                                    state[pn][key] = val.toFixed(0);
                                } else if (state[pn][key]) {
                                    delete state[pn][key];
                                }
                            }
                        } else {
                            state[pane] = state[pane] || {};
                            if (typeof val === 'number') {
                                state[pane][key] = val.toFixed(0);
                            } else if (state[pane][key]) {
                                delete state[pane][key];
                            }
                        }
                        if (!_rszrOnStop) {
                            var id = lout[0].id;
                            $jui.ajax.storeState($jui.widget.getCleanID(id), state)
                        }
                    }
                }catch(e){ $jui.dbgAlert('lout.storeState()', e.message); }
            }
        }

        function resizerOpts( opts ) {
            opts = opts || {};
            $jq.extend(opts, {
                start: function(event, ui) {
                    juiwindata.JuiResizing = true;
                    _rszrResizing = true;
                },
                resize: function(event, ui){
                    $jq(this).data('instance').resizeAll();
                    return false;
                },
                stop: function(event, ui) {
                    var lout = $jui.widget.getSelf(this)
                    ,   juisize = lout.juisize
                    ,   state   = lout.juistate
                    ,   height  = ui.size.height
                    ,   width   = ui.size.width
                    ;
                    _rszrResizing = false;
                    _rszrOnStop = true;
                    state.resizer = juisize.calcSizeVal(height, width, juisize);
                    lout.juiResize();
                    _rszrOnStop = false;
                    if (!juiwindata.oninit && !lout.rflags.nostorestate && state) {
                        $jui.ajax.storeState($jui.widget.getCleanID(this.id), state)
                    }
                    juiwindata.JuiResizing = false;
                }
            });
            return opts;
        }

       function resizerFuncs () {
            return {
                ready: function(data) {
                    // rszr===lout
                    data.rszr.data('instance').resizeAll();
                }
            }
        }

    }

};


