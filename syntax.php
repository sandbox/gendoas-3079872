<?php
/**
 * DokuWiki Plugin juilayout (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once DOKU_PLUGIN.'juiwidget/syntax.php';

class syntax_plugin_juilayout extends syntax_plugin_juiwidget {

    function getType() { return 'substition';}
    function getPType() { return 'block';}
    function getSort() { return 302; }


    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<juilayout.+?</juilayout>',$mode,'plugin_juilayout');
    }

    private function _getPanes() {
        return array('center', 'north', 'east', 'south', 'west');
    }
    public function handle($match, $state, $pos, $handler){

        // if ( $this->getIsNesting() ) >>> don't abort, data are needed (instructions) !!!

        // options for that widget
        $plugin = $this->getPluginName();
        $data = array('match' => $match);
        list($opts, $content) = explode('>', substr($match, 10, -12), 2);
        // options for the widget itself
        $data['opts'][$plugin] = $opts;
        // more options
        //JUIFIX:: -------- ?? more-options ???
//        $data['opts']['resizer'] = array();
        $content = $this->extractOptions($content, $data['opts']);


        // sections
        $pattern = '&<layout-section.+?</layout-section>&is';
        preg_match_all($pattern, $content, $sections);
        $data_sects = array();
        $idx = 0;
        foreach($sections[0] as $section){
            list($opts, $content) = explode('>', substr($section, 15, -17), 2);

            // options for that section itself
            $data_sects[++$idx]['opts']['head'] = $opts;
            // more options
            $data_sects[$idx]['content'] = $this->extractOptions($content, $data_sects[$idx]['opts']);
        }
        $data['sections'] = $data_sects;

                    $msg = "match=$match";
//                    juiDbgMsg("..........handle:: $msg", -1);
//                                $match = $data['match'];
//                                msg('render:: ' . $match);

        return $data;
    }

    public function render($mode, $renderer, $data) {

        // for ajax prevent caching
        $this->ajaxSetCache($renderer);

        if ($mode != 'metadata' && $mode != 'xhtml') {
            return false;
        }

        if ($this->isPreview() && mode == 'metadata') {
            return false;
        }
        
        // nesting not allowed,
        if ($this->getIsNesting()) {
            if ($mode == 'xhtml') {
                // show message-page
                $renderer->doc .= $this->showMsgHtml(array('msgkey' => 'no_nesting'));
            }
            return true;
        }

        // section needed
        if ( !count($data['sections']) ) {
            if ($mode == 'metadata') {
                juiDbgMsg('layout-pane required.');
            }
            return false;
        }

        // parse options, widget and its sections
        $data['opts'] = $this->parseOptionString($data['opts'], 'widget');
        $allowed_panes =$this->_getPanes();
        $sections = array();
        foreach ($data['sections'] as $section) {
            $pane = $this->_optsGetPaneName($section['opts']['head']);
            if (!in_array($pane, $allowed_panes)) continue;
            
            $section['opts'] = $this->parseOptionString($section['opts'], 'section', $pane);
            $pane = $section['opts']['head']['pane'];
            if ( isset($pane) ) {
                unset($section['opts']['head']['pane']);
                $sections[$pane] = $section;
            }
        }

        // center-pane needed
        if ( !isset($sections['center']) ) {
            if ($mode == 'metadata') {
                juiDbgMsg('layout center-pane required.');
            }
            return false;
        }

        $data['sections'] = $sections;

        $plugin = $this->getPluginName();
        $uid = $data['opts'][$plugin]['uid'];
        $juiid = $this->getWidgetUID($uid, $match, $plugin);;
        $data['plugin'] = $plugin;
        $data['juiid'] = $juiid;


        if ($mode == 'metadata') {
            return $this->prepareJuiVars($juiid, $data, $renderer);
        } else /*($mode == 'xhtml')*/ {
            if ($this->isPreview() || $this->isSyspage()) {
                $this->prepareJuiVars($juiid, $data, $renderer);
            }
            return $this->_renderXhtml($mode, $renderer, $data);
        }
    }
    
    public function prepareJuiVarsAfter(&$jsvars) {
        $plugin = $this->getPluginName();
        if ($jsvars[$plugin]['opts']) {
            $defaults = $jsvars[$plugin]['opts'];
            unset($jsvars[$plugin]['opts']);
        }
        
        $jsvars[$plugin]['opts'] = array();
        $opts =& $jsvars[$plugin]['opts'];
        if (isset($defaults)) $opts['defaults'] = $defaults;
        $pvars = $jsvars[$plugin]['panes'];
        if (!isset($jsvars['sections']) && !isset($pvars)) return;
        
        $panes = array('center', 'north', 'south', 'west', 'east');
        foreach ($panes as $pane) {
            if (isset($pvars[$pane])) {
                $opts[$pane] = $pvars[$pane];
            }
            if (isset($jsvars['sections'][$pane]) && isset($jsvars['sections'][$pane]['head'])) {
                if (isset($head)) unset($head);
                $head =& $jsvars['sections'][$pane]['head'];
                if (isset($head['opts'])) {
                    $opts[$pane] = is_array($opts[$pane]) 
                                        ? array_merge($opts[$pane], $head['opts']) 
                                        : $head['opts'];
                    unset($head['opts']);
                }
            }
        }
        if(isset($jsvars[$plugin]['panes'])) unset($jsvars[$plugin]['panes']);
    }
    
    
    private function _optsGetPaneName($opts) {
        $opts = juiGetOptionsArray($opts, '*');
        return $opts['pane'] ? $opts['pane'] : '';
    }

    private function _renderXhtml($mode, &$renderer, &$data) {
        global $ID;

        if($mode != 'xhtml') {
            return false;
        }

        $plugin = $data['plugin'];
        $juiid  = $data['juiid'];
        $scheme = $data['opts'][$plugin]['scheme'];

//                    $msg = "mode=$mode, juiid= $juiid";
//                    juiDbgMsg("..........render:: $msg", 0);


        $doc .= juiDbgMsgHtml("\n<!-- layout-start -->\n");
        $p = array();
        $p['id']    = $juiid;
        $p['class'] = $this->getWidgetClasses($scheme);
        $p['page']  = $ID;
        $att = buildAttributes($p);
        $doc .= "<div $att>\n";


        // sections
        foreach( $data['sections'] as $pane => $section ) {
            $doc .= juiDbgMsgHtml(DOKU_TAB . "<!-- layout-pane-$pane-start -->\n");

            // section-content
            $p = array();
            $p['id']    = $this->getExtUID($juiid, $pane);
//            $p['class'] = CLS_JUICONTENT . $plugin . '-content';
//            $p['class'] = CLS_JUICONTENT . "$plugin-pane ui-layout-$pane";
            $p['class'] = CLS_JUICONTENT . "$plugin-pane ui-layout-$pane";
            $att = buildAttributes($p);
            $doc .= DOKU_TAB . "<div $att>\n";

            // parse the content
            $html = $this->renderJuiContent($section['content'], $juiid);
            $doc .= DOKU_TAB . DOKU_TAB . $html . "\n";

            // end section
            $doc .= DOKU_TAB . "</div>" . juiDbgMsgHtml("<!-- layout-pane-$pane-end -->\n");
        }
        $doc .= '</div>';
        $doc .= juiDbgMsgHtml("<!-- layout-end -->\n");

        $renderer->doc .= $doc;
        return true;
    }




    /*
     * Testfunktionen
     */
    private function testGetContent($file) {
        //return file_get_contents(DOKU_PLUGIN_JUIACC . '___test/' . $file);
    }

}

// vim:ts=4:sw=4:et:
