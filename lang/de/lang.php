<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']   		= '~~Layout-Elemente können nicht geschachtelt werden~~';
$lang['head_caption']   	= 'Feld';
$lang['js']['juilouttest']	= 'Dies ist eine JS-Testvariable'; 
