<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']         = '~~nested layouts not supported~~';
$lang['head_caption']   	= 'Pane';
$lang['js']['juilouttest']	= 'This is a js-testvariable'; 
