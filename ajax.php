<?php
/**
 * AJAX call handler for juilayout plugin
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     FIXME
 */

if(!defined('DOKU_INC')) define('DOKU_INC',dirname(__FILE__).'/../../../');
require_once(DOKU_INC.'lib/plugins/juiwidget/ajax.php');
