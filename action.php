<?php
/**
 * DokuWiki Plugin juilayout (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');

require_once DOKU_PLUGIN.'action.php';
require_once DOKU_PLUGIN_JUI.'juiutils.php';

class action_plugin_juilayout extends DokuWiki_Action_Plugin {

    public function register(Doku_Event_Handler $controller) {
        $controller->register_hook('JUIWIDGET_READY', 'BEFORE', $this, 'handleJQueryuiReady');
        juiSetJuiPlugins($this->getPluginName());
    }

    public function handleJQueryuiReady(Doku_Event &$event, $param) {

        if (juiGetIsAjaxCall()) {
            // not for ajax-call
            return;
        }

        // juilayoutOnJqReady()         --->to find in ./script.js
        // wrapped in juiOnJqReady      --->to find in ../juiwidget/script.js
        $func = $this->getPluginName();
        $data = '{}';
        $event->data[] = "juiOnJqReady('$func', $data);";
//        $event->data[] = '$jui.onJqReady' . "('$func', $data);";
    }
    


}

// vim:ts=4:sw=4:et:
